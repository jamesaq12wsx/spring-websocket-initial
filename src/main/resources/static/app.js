/**
 * Created by shaochinlin on 2018/10/31.
 */
//測試沒有websocket的環境
WebSocket = undefined;

var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/ws');
    var name = $('#name').val();
    stompClient = Stomp.over(socket);

    stompClient.heartbeat.outgoing = 10000; // client will send heartbeats every 10000ms
    stompClient.heartbeat.incoming = 10000; // client want recieve heartbeat every 10000ms
    
    stompClient.connect({login: name}, connectSuccessHandler);
}

function connectSuccessHandler(frame) {
    setConnected(true);

    console.log('Connected: ' + frame);

    //for 通知
    stompClient.subscribe('/topic', messageHandler);

    //for 訊息
    stompClient.subscribe('/chat', messageHandler);

    // for private message
    // stompClient.subscribe('/queue/private', privateMessageHandler);

    // stompClient.subscribe('/queue', messageHandler);

    setTimeout(sendGreeting(), 2000);
}

function messageHandler(stompMessage){
    var response = JSON.parse(stompMessage.body);
    var jsonMessage = response.map;

    console.log('response', jsonMessage);

    switch (jsonMessage.type){
        case 'message':
            showGreeting(jsonMessage);
            break;
        case 'private-message':
            showPrivateMessage(jsonMessage);
            break;
    }
}

function privateMessageHandler(message){
    var response = JSON.parse(message.body);

    console.log('response', response);

    switch (response.type){
        case 'message':
            showPrivateMessage(response);
            break;
    }
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendGreeting() {

    var user = $('#name').val();

    var jsonMessage = {
        type : 'message',
        user: user,
        to: '',
        message: 'Welcome ' + user + ' join the room'
    }

    var jsonStr = JSON.stringify(jsonMessage);

    stompClient.send("/app/hello", {}, jsonStr);
}

function sendMessage(){
    var to = $('#to').val() !== "" ? $('#to').val() : "";
    var message = $('#message').val();
    var name = $('#name').val();
    var alertMessage = "";

    if (message.message === ""){
        alertMessage = "Cannot send empty message";
        alert(alertMessage);
    }
    else{
        var sendMessage = {
            to: to,
            user: name,
            type: to === "" ? "message" : "private-message",
            message: message
        };
        var jsonMessage = JSON.stringify(sendMessage);

        if(sendMessage.to !== ""){
            stompClient.send('/app/chat.' + sendMessage.to, {}, jsonMessage);
        }
        else{
            stompClient.send('/app/chat', {}, jsonMessage);
        }

        $('#message').val('');
    }
}

function showGreeting(message) {
    $("#greetings").prepend("<tr><td>[" + message.user + "] " + message.message + "</td></tr>");
}

function showPrivateMessage(message) {
    $("#greetings").prepend("<tr><td>[" + message.user + "->" + message.to +  "] " + message.message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendMessage(); });
});

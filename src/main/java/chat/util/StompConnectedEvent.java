package chat.util;

import chat.model.SubscribeListener;
import chat.model.User;
import org.apache.catalina.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.socket.messaging.*;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import chat.model.ConnectListener;

/**
 * Created by shaochinlin on 2018/10/31.
 */


public class StompConnectedEvent {

    Logger logger = LoggerFactory.getLogger(StompConnectedEvent.class);
    ParticipantRepository participantRepository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    public StompConnectedEvent(ParticipantRepository participantRepository){
        this.participantRepository = participantRepository;
    }
    @EventListener
    private void handleSubscribe(SessionSubscribeEvent event){
        logger.debug(">>>[SUBSCRIBE EVENT]\n{}", event);
    }

    @EventListener
    private void handleSessionConnected(ConnectListener event) {
        logger.debug(">>>[SESSION CONNECTED EVENT]");
//        String name = event.getUser().getName();
//        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
//        String username = headers.getNativeHeader("login").get(0);

//        String sessionId = headers.getSessionId();

//        logger.debug(">>>[SESSION CONNECTED EVENT]sessionID:{}, username: {}", sessionId, username);

//        User user = new User(name, username, sessionId);

//        participantRepository.add(user);

//        messagingTemplate.convertAndSend("/topic", "Welcome " + username);
//        messagingTemplate.convertAndSendToUser(sessionId, "/topic", "Connect Success", headers.getMessageHeaders());
    }

    @EventListener
    private void handleSessionDisconnect(SessionDisconnectEvent event) {
        String sessionId = event.getSessionId();

        logger.debug(">>>[SESSION DISCONNECTED] Someone disconnected:{}", sessionId);

        participantRepository.deleteBySession(sessionId);

//        messagingTemplate.convertAndSend("topic/notification", sessionId + " is leaving");
    }

}

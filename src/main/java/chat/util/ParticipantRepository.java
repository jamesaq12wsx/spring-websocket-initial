package chat.util;

import chat.model.User;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shaochinlin on 2018/10/31.
 */
public class ParticipantRepository {
    private Map<String, User> usersSession = new ConcurrentHashMap<>();

    private Map<String, User> usersName = new ConcurrentHashMap<>();

    private Map<String, User> usersUsername = new ConcurrentHashMap<>();

    public Map<String, User> getActiveUser(){
        return this.usersSession;
    }

    public User getBySession(String session){
        return usersSession.get(session);
    }

    public User getByName(String name){
        return usersName.get(name);
    }

    public User getByUsername(String username){ return usersUsername.get(username); }

    public void add(User user){
        usersSession.put(user.getSessionID(), user);
        usersName.put(user.getName(), user);
        usersUsername.put(user.getUsername(), user);
    }

    public void deleteBySession(String sessionId){
        if(usersSession.keySet().contains(sessionId)){
            User user= usersSession.get(sessionId);
            usersSession.remove(sessionId);
            usersName.remove(user.getName());
            usersUsername.remove(user.getUsername());
        }
    }

    public void deleteByName(String name) {
        if (usersName.keySet().contains(name)) {
            User user = usersSession.get(name);
            usersSession.remove(user.getSessionID());
            usersName.remove(name);
            usersUsername.remove(user.getUsername());
        }
    }
}

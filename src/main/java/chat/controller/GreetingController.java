package chat.controller;

import chat.model.ChatMessage;
import chat.model.ResponseMessage;
import chat.model.User;
import chat.util.ParticipantRepository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.tools.corba.se.idl.StringGen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.slf4j.*;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@Controller
public class GreetingController {

    Logger logger = LoggerFactory.getLogger(GreetingController.class);

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ParticipantRepository activeUsers;

    private static final Gson gson = new GsonBuilder().create();

    private Map<String, String> message = new HashMap<>();

    @MessageMapping("/hello")
    // @SendTo("/topic/greetings")
    public void greeting(@Payload ChatMessage message, SimpMessageHeaderAccessor headerAccessor) throws Exception{
        logger.debug(">>>[GREETING]");
        // String sessionID = headerAccessor.getSessionId();
        JsonParser parser = new JsonParser();
//        JsonObject o = parser.parse("{\"a\": \"A\"}").getAsJsonObject();

//        Type type = new ChatMessage().getMessage().getClass();
        String jsonMessage = gson.toJson(message);
        JsonObject jsonObMessage = parser.parse(jsonMessage).getAsJsonObject().get("map").getAsJsonObject();

        logger.debug(">>>[GREETING] JSON MESSAGE:{}", jsonMessage);
//        logger.debug(">>>[GREETING] JSON O MESSAGE TEXT:{}", jsonObMessage.getAsJsonObject("message").get("text").getAsString());

        Thread.sleep(1000);

        messagingTemplate.convertAndSend("/queue", message);
    }

    @MessageMapping("/chat")
    public void handleChat(ChatMessage message, SimpMessageHeaderAccessor headerAccessor) throws Exception{
        logger.debug(">>>[CHAT MESSAGE]");

        // String sessionID = headerAccessor.getSessionId();

        // String toUser = message.getUser();

        // String userMessage = message.getMessage();

//        logger.debug(">>>[CHAT MESSAGE] {}", message.toString());

        JsonParser parser = new JsonParser();
//        JsonObject o = parser.parse("{\"a\": \"A\"}").getAsJsonObject();

//        Type type = new ChatMessage().getMessage().getClass();
        String jsonMessage = gson.toJson(message);
        JsonObject jsonObMessage = parser.parse(jsonMessage).getAsJsonObject().get("map").getAsJsonObject();

//        logger.debug(">>>[CHAT MESSAGE] JSON MESSAGE:{}", jsonMessage);
//        logger.debug(">>>[CHAT MESSAGE] JSON O MESSAGE:{}", jsonObMessage.get("message"));

        messagingTemplate.convertAndSend("/queue", message);

//        messagingTemplate.convertAndSendToUser(message.getTo(), "/queue", message.getMessage());
    }

    @MessageMapping("/chat.{user}")
    public void handlePrivateChat(@Payload ChatMessage message, SimpMessageHeaderAccessor headerAccessor) throws Exception{
        logger.debug(">>>[PRIVATE CHAT MESSAGE]");

        JsonParser parser = new JsonParser();
        SimpMessageHeaderAccessor responseAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);

        String jsonMessage = gson.toJson(message);
        JsonObject jsonObMessage = parser.parse(jsonMessage).getAsJsonObject().get("map").getAsJsonObject();

        User toUser = activeUsers.getByName(jsonObMessage.get("to").getAsString());
//        String destinateSessionID = toUser.getSessionID();
//        responseAccessor.setSessionId(destinateSessionID);
//        responseAccessor.setLeaveMutable(true);

        if(toUser != null){
            String destinateName = toUser.getName();
            responseAccessor.setSessionId(destinateName);
            responseAccessor.setLeaveMutable(true);

//            messagingTemplate.convertAndSend("/queue/"+toUser.getName(), message);
            messagingTemplate.convertAndSendToUser(destinateName, "/queue/private", message, responseAccessor.getMessageHeaders());
        }
    }

//    @SubscribeMapping("chat.{user}")
    public void hello(@DestinationVariable String user){
        logger.debug("[SUBSCRIBE {}]", user.toUpperCase());
    }

//    // @MessageMapping(value = "/hello")
//    @SubscribeMapping("/{user}")
//    public void hello(@DestinationVariable String user) {
//        logger.debug("[HELLO]");
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        User userSession = activeUsers.getByName(user);
//
//        JsonObject response = new JsonObject();
//        response.addProperty("id", "subscribeResponse");
//        response.addProperty("response", "/queue/" + user + " subscribe success");
//
//        if(userSession != null){
//            response.addProperty("sessionID", userSession.getSessionID());
//        }else{
//            response.addProperty("sessionID", "User did not login");
//        }
//
//        messagingTemplate.convertAndSend("/queue/"+user, response);
////        return response;
//    }
}

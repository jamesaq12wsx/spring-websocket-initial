package chat.controller;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import org.slf4j.*;

/**
 * Created by shaochinlin on 2018/11/6.
 */


@Controller
public class SubscribeController {

    Logger logger = LoggerFactory.getLogger(SubscribeController.class);

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @SubscribeMapping("/chat")
    public void hello(SimpMessageHeaderAccessor headerAccessor){
//        logger.debug("[HELLO] USER:\n{}", user.toUpperCase());

        String sessionID = headerAccessor.getSessionId();

        logger.debug("[SUBSCRIBE MAPPING] USER:{}", sessionID);

        JsonObject responseObject = new JsonObject();
        responseObject.addProperty("type", "subscribeControllerRes");

        String responseStr = responseObject.toString();
        JsonObject wrapMessage = new JsonObject();
        wrapMessage.addProperty("map", responseStr);

        messagingTemplate.convertAndSend("/chat", wrapMessage.toString());
        messagingTemplate.convertAndSend(String.format("/chat/queue/a-{}", sessionID), wrapMessage.toString(), createHeaders(sessionID));
    }

    public MessageHeaders createHeaders(String sessionId){
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
//        headerAccessor.setDestination(String.format("/queue/queue/a-{}", sessionId));
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        return headerAccessor.getMessageHeaders();
    }
}

package chat.model;

import java.security.Principal;
import java.util.Date;

/**
 * Created by shaochinlin on 2018/10/31.
 */
public class User implements Principal {

    private final String name;

    private String username;

    private String sessionID;

    private Date date;

    public User(String name, String username, String sessionID){

        this.name = name;

        this.username = username;

        this.sessionID = sessionID;

        this.date = new Date();
    }

    @Override
    public String getName(){
        return this.name;
    }

    public String getUsername() {
        return username;
    }

    public String getSessionID(){
        return this.sessionID;
    }

    public void setSessionID(String sessionID){ this.sessionID = sessionID;}

    public  Date getDate(){
        return this.date;
    }
}

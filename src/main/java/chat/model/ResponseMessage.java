package chat.model;

/**
 * Created by shaochinlin on 2018/11/1.
 */
public class ResponseMessage {
    
    private String fromUser;

    private String message;

    public ResponseMessage(String fromUser) {
        this.fromUser = fromUser;
        this.message = "";
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

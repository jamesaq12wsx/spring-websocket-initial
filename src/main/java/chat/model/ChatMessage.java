package chat.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shaochinlin on 2018/11/1.
 */


/**
 * 接收使用者傳來的訊息
 */
public class ChatMessage {

    private Map<String, String> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, String> any(){
        return this.map;
    }

    public Map<String, String> getMap() {
        return this.map;
    }

    @JsonAnySetter
    public void setMap(String key, String value) {
        map.put(key, value);
    }

    @Override
    public String toString() {
        return "Map [map=" + map + "]";
    }
}

package chat;

/**
 * Created by shaochinlin on 2018/10/31.
 */

import chat.util.ParticipantRepository;
import chat.util.StompConnectedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.config.WebSocketMessageBrokerStats;
import javax.annotation.PostConstruct;

import org.slf4j.*;


@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Autowired
    private WebSocketMessageBrokerStats webSocketMessageBrokerStats;

    @Autowired
    private ParticipantRepository participantRepository;

    Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);

    @PostConstruct
    public void init() {
        logger.debug(">>>PostConstruct");

//      Broker狀態顯示頻率
        webSocketMessageBrokerStats.setLoggingPeriod(30 * 1000);
    }

    @Bean
    @Description("Tracks user presence (join / leave) and broacasts it to all connected users")
    public StompConnectedEvent stompConnectedEvent() {
        StompConnectedEvent stompEvent = new StompConnectedEvent(participantRepository());
        return stompEvent;
    }

    @Bean
    @Description("Keeps connected users")
    public ParticipantRepository participantRepository() {
        return new ParticipantRepository();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        logger.debug(">>>[CONFIGUE MESSAGE BROKER]");
        // registry.enableSimpleBroker("/topic");
        registry.enableSimpleBroker("/chat", "/topic");
//        registry.setApplicationDestinationPrefixes("/user");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        logger.debug(">>>[REGISTER STOMP ENDPOINT]");

        registry.addEndpoint("/ws").withSockJS();
//        registry.addEndpoint("/chat").withSockJS();
    }
}
